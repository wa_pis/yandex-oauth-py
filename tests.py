import unittest
from yandex_oauth import OAuthYandex

class OauthTestCase(unittest.TestCase):

    def setUp(self):
        self.oa = OAuthYandex('key1','key2')

    def test_get_code(self):
        self.assertEqual(self.oa.get_code(), 'https://oauth.yandex.ru/authorize?response_type=code&client_id=key1')

    def test_get_token(self):
        """
        need mock obk requests
        """
        pass

    def test_get_basic_auth_token(self):
        self.auth = OAuthYandex.get_basic_auth_token('key1', 'key2')
        self.assertEqual(self.auth, 'Basic a2V5MTprZXky')

if __name__ == "__main__":
    unittest.main()
